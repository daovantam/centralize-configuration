FROM openjdk:8
ADD target/centralize-configuration-0.0.1.jar centralize-configuration-0.0.1.jar
EXPOSE 8888
ENTRYPOINT ["java", "-jar", "centralize-configuration-0.0.1.jar"]